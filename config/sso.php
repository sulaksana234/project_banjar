<?php

return [
	'jwt_key' => env('JWT_KEY'),
	'url' => env('SSO_URL', 'http://sso-mm.local.com'),
	'port' => env('SSO_PORT', '80'),
	'env_api' => env('ENVAPI', 'laptop'),
	'url_pr' => env('URLPR', 'http://pr.local.com:82/'),
	'url_sso' => env('URLSSO', 'http://sso-mm.local.com:82'),


];