@extends('layouts.templateadmin')
@section('title','Dashboard')
@section('content')
<style>
    .card {
        padding: 1em 0.5em 0.5em;
        border-radius: 2em;
        text-align: center;
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    }

    .card img {
        width: 65%;
        border-radius: 50%;
        margin: 0 auto;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
    }

    .card .card-title {
        font-weight: 700;
        font-size: 1.5em;
    }

    .card .btn {
        border-radius: 2em;
        background-color: teal;
        color: #ffffff;
        padding: 0.5em 1.5em;
    }

    .card .btn:hover {
        background-color: rgba(0, 128, 128, 0.7);
        color: #ffffff;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
    }

    .container {
        max-width: 650px;
        width: 100%;
        padding: 30px;
        background: #fff;
        border-radius: 20px;
        box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;
    }

    .drag-area {
        height: 400px;
        border: 3px dashed #e0eafc;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        margin: 10px auto;
    }

    h3 {
        margin-bottom: 20px;
        font-weight: 500;
    }

    .drag-area .icon {
        font-size: 50px;
        color: #1683ff;
    }

    .drag-area .header {
        font-size: 20px;
        font-weight: 500;
        color: #34495e;
    }

    .drag-area .support {
        font-size: 12px;
        color: gray;
        margin: 10px 0 15px 0;
    }

    .drag-area .button {
        font-size: 20px;
        font-weight: 500;
        color: #1683ff;
        cursor: pointer;
    }

    .drag-area.active {
        border: 2px solid #1683ff;
    }

    .drag-area img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    .download {
        background-color: #4CAF50;
        border: none;
        color: white;
        padding: 6px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 3px 1px;
        border-radius: 12px;
        cursor: pointer;
        width: 100%;
    }
</style>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<link href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>

<div class="main-panel">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin" style="height:200px;">
                <div class="card">
                    <div class="card" style="background-color: #000080;">

                        <h1 style="color:#ffffff">Data User</h1>
                        <a class="btn" style="color: #ffffff;" href="javascript:void(0)" id="createNewUser" onclick="tambahUser()"><i class="fas fa-plus-circle"></i> Add User</a>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered data-user table-responsive">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th width="280px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                    </div>



                </div>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="modalTambahUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true">
    <div class="modal-dialog modal-dialog-slideout" role="document">
        <div class="modal-content" style="   border-radius: 2em;padding:20px;">
            <div class="modal-header" style="border-radius: 2em;background-color:#000080;color:#ffffff;">
                <h5 class="modal-title" id="exampleModalLabel">Form User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="row g-3 needs-validation" novalidate>
                    <div class="col-md-6">
                        <label for="firsName" class="form-label">First name</label>
                        <input type="text" class="form-control form-control-sm" id="firsName" name="firsName" value="" required>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                        <div class="invalid-feedback">
                            Data First Name Belum Terisi
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="validationCustom02" class="form-label">Last name</label>
                        <input type="text" class="form-control form-control-sm" id="lastName" name="lastName" value="" required>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                        <div class="invalid-feedback">
                            Data Last Name Belum Diinputkan
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="username" class="form-label">Username</label>
                        <div class="input-group has-validation">
                            <span class="input-group-text" id="username">@</span>
                            <input type="text" class="form-control form-control-sm" id="username" name="username" aria-describedby="inputGroupPrepend" required>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                            <div class="invalid-feedback">
                                Username belum diinputkan
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="email" class="form-label">Email</label>
                        <div class="input-group has-validation">
                            <input type="email" class="form-control form-control-sm" id="email" name="email" aria-describedby="inputGroupPrepend" required>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                            <div class="invalid-feedback">
                                Email belum diinputkan
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="password" class="form-label">Password</label>
                        <div class="input-group has-validation">
                            <input type="password" class="form-control form-control-sm" id="password" name="password" aria-describedby="inputGroupPrepend" required>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                            <div class="invalid-feedback">
                                Password belum diinputkan
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label for="confirm_password" class="form-label">Confirm Password</label>
                        <div class="input-group has-validation">
                            <input type="confirm_password" class="form-control form-control-sm" id="confirm_password" name="confirm_password" aria-describedby="inputGroupPrepend" required>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                            <div class="invalid-feedback">
                                Confirm Password dan Password belum sama
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <label for="confirm_password" class="form-label">Alamat</label>
                        <div class="input-group has-validation">
                            <input type="confirm_password" class="form-control form-control-sm" id="confirm_password" name="confirm_password" aria-describedby="inputGroupPrepend" required>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                            <div class="invalid-feedback">
                                Confirm Password dan Password belum sama
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label for="confirm_password" class="form-label">Foto Profile</label>

                        <div class="container">
                            <div class="drag-area">
                                <div class="icon">
                                    <i class="fas fa-images"></i>
                                </div>
                                <span class="header">Drag & Drop</span>
                                <span class="header">or <span class="button">browse</span></span>
                                <input type="file" hidden />
                                <span class="support">Supports: JPEG, JPG, PNG</span>
                            </div>
                         
                        </div>
                    </div>





            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>

        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function(form) {
                form.addEventListener('submit', function(event) {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            });


        var table = $('.data-user').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('user.index') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]
        });


    });

    function tambahUser() {
        $('#modalTambahUser').modal('show');
    }


    //drag and drop
    const dropArea = document.querySelector('.drag-area');
    const dragText = document.querySelector('.header');

    let button = dropArea.querySelector('.button');
    let input = dropArea.querySelector('input');

    let file;

    button.onclick = () => {
        input.click();
    };

    // when browse
    input.addEventListener('change', function() {
        file = this.files[0];
        dropArea.classList.add('active');
        displayFile();
    });

    // when file is inside drag area
    dropArea.addEventListener('dragover', (event) => {
        event.preventDefault();
        dropArea.classList.add('active');
        dragText.textContent = 'Release to Upload';
        // console.log('File is inside the drag area');
    });

    // when file leave the drag area
    dropArea.addEventListener('dragleave', () => {
        dropArea.classList.remove('active');
        // console.log('File left the drag area');
        dragText.textContent = 'Drag & Drop';
    });

    // when file is dropped
    dropArea.addEventListener('drop', (event) => {
        event.preventDefault();
        // console.log('File is dropped in drag area');

        file = event.dataTransfer.files[0]; // grab single file even of user selects multiple files
        console.log("file",file);
        // console.log(file);
        displayFile();
    });

    function displayFile() {
        let fileType = file.type;
        // console.log(fileType);

        let validExtensions = ['image/jpeg', 'image/jpg', 'image/png'];

        if (validExtensions.includes(fileType)) {
            // console.log('This is an image file');
            let fileReader = new FileReader();

            fileReader.onload = () => {
                let fileURL = fileReader.result;
                // console.log(fileURL);
                let imgTag = `<img src="${fileURL}" alt="">`;
                dropArea.innerHTML = imgTag;
            };
            fileReader.readAsDataURL(file);
        } else {
            alert('This is not an Image File');
            dropArea.classList.remove('active');
        }
    }
</script>
@endsection